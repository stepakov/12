<?php

    class News extends CI_Controller
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->model('news_model' );
        }

        public function index()
        {
            $data[ 'news' ] = $this->news_model->get_news();

            $data[ 'title' ] = 'Archive news';

            $this->load->view( 'templates/header', $data );
            $this->load->view( 'news/index', $data );
            $this->load->view( 'templates/footer' );
        }

        public function view( $slug = NULL )
        {
            $data[ 'news_item' ] = $this->news_model->get_news( $slug );
            if ( empty( $data[ 'news_item' ] ) )
            {
                show_404();
            }
            $data[ 'title' ] = $data[ 'news_item' ][ 'title' ];

            $this->load->view( 'templates/header', $data );
            $this->load->view( 'news/view', $data );
            $this->load->view( 'templates/footer' );
        }

        public function create()
        {
            $this->load->helper( 'form' );
            $this->load->library( 'form_validation' );

            $this->form_validation->set_rules( 'title', 'Title', 'required' );
            $this->form_validation->set_rules( 'text', 'text', 'required' );

            $data[ 'title' ] = 'News creating';

            if( $this->form_validation->run() === FALSE )
            {
                $this->load->view( 'templates/header', $data );
                $this->load->view( 'news/create' );
                $this->load->view( 'templates/footer' );
            }
            else
            {
                if ( $this->news_model->create_news() )
                {
                    $this->load->view( 'news/success' );
                }
            }
        }

        public function edit( $slug = NULL )
        {
            $this->load->helper( 'form' );
            $this->load->library( 'form_validation' );

            $this->form_validation->set_rules( 'title', 'Title', 'required' );
            $this->form_validation->set_rules( 'text', 'text', 'required' );

            $data[ 'news_item' ] = $this->news_model->get_news( $slug );
            if ( empty( $data[ 'news_item' ] ) )
            {
                show_404();
            }
            $data[ 'title' ] = "Edit news " . $data[ 'news_item' ][ 'title' ];

            if( $this->form_validation->run() === FALSE )
            {
                $this->load->view( 'templates/header', $data );
                $this->load->view( 'news/edit', $data );
                $this->load->view( 'templates/footer' );
            }
            else
            {
                if ( $this->news_model->update_news() )
                {
                    $this->load->view( 'news/success' );
                }
            }
        }

        public function delete( $slug )
        {
            $data[ 'news_item' ] = $this->news_model->get_news( $slug );
            if ( empty( $data[ 'news_item' ] ) )
            {
                show_404();
            }

            if ( $this->news_model->delete( $data[ 'news_item' ][ 'id' ] ) )
            {
                $this->load->view( 'news/success' );
            }
        }
    }
