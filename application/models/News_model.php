<?php

    class News_model extends CI_Model
    {
        public function __construct()
        {
            $this->load->database();
        }

        public function get_news( $slug = NULL )
        {
            if ( $slug === NULL )
            {
                $query = $this->db->get( 'news' );
                return $query->result_array();
            }

            $query = $this->db->get_where( 'news', array( 'slug' => $slug ) );
            return $query->row_array();
        }

        public function create_news()
        {
            $this->load->helper( 'url' );
            $title = $this->input->post( 'title' );
            $text = $this->input->post( 'text' );
            $slug = url_title( $title, 'dash', TRUE );

            $data = array(
                'title' => $title,
                'text' => $text,
                'slug' => $slug
            );

            return $this->db->insert( 'news', $data );
        }

        public function update_news()
        {
            $this->load->helper( 'url' );
            $title = $this->input->post( 'title' );
            $text = $this->input->post( 'text' );
            $slug = url_title( $title, 'dash', TRUE );
            $id = $this->input->post( 'id' );

            $data = array(
                'title' => $title,
                'text' => $text,
                'slug' => $slug
            );

            var_dump( $data );

            return $this->db->update( 'news', $data, array( 'id' => $id ) );
        }

        public function delete( $id )
        {
            return $this->db->delete( 'news', array( 'id' => $id ) );
        }
    }
