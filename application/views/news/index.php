<a href="/news/create">Create news</a> <br>

<?php foreach ( $news as $news_item ) : ?>
    <h2><?php echo $news_item[ 'title' ]; ?></h2>
    <p><?php echo $news_item[ 'text' ]; ?></p>
    <p><a href="/news/view/<?php echo $news_item[ 'slug' ]; ?>">View news</a></p>
    <p><a href="/news/edit/<?php echo $news_item[ 'slug' ]; ?>">Edit news</a></p>
    <p><a href="/news/delete/<?php echo $news_item[ 'slug' ]; ?>">Delete news</a></p>
    <br>
<?php endforeach; ?>
